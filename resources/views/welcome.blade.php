<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" ng-app="passwordManagerApp">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Password Manager</title>
        <link href="css/app.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <script src="app/lib/angular/angular.min.js"></script>
        <script src="app/lib/angular/lodash.min.js"></script>
        <script src="app/lib/angular/restangular.min.js"></script>
        <script src="app/lib/angular/angular-local-storage.min.js"></script>
        <script src="app/lib/angular/angular-route.min.js"></script>
        <script src="app/lib/angular/ng-webcrypto.js"></script>
        <script src="app/lib/angular/cryptojscipher.js"></script>
        <script src="app/lib/angular/ng-angularjs-crypto.js"></script>
        <script src="app/lib/angular/aes.min.js"></script>
        <script src="app/services/pbkdf2.js"></script>
        <script src="app/lib/angular/mob-ecb.js"></script>
        <script src="app/app.js"></script>
        <script src="app/services/services.js"></script>
        <script src="app/controllers/controllers.js"></script>

        <style>
            
            li {
                padding-bottom: 8px;
            }

        </style>
    </head>

    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Password Manager</h1>
                </div>
            </div>
        </div>

        <div ng-view>
        </div>

        <script src="js/all.js"></script>
    </body>
</html>
