<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'public_key','encrypted_key'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    public static $validation_rules = [
        'public_key'=>'required'
    ];

    public static $signup_validation_rules = [
        'public_key'=>'required|unique:users'
   ];
}
