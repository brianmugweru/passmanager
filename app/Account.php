<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Adldap\Laravel\Traits\HasLdapUser;

class Account extends Model
{
    //
    protected $fillable = [
        'user_id','uid','mail','site_url','password'
    ];

    public function password()
    {
        return $this->hasOne('App\Password','account_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
