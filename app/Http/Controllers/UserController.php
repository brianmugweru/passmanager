<?php

namespace App\Http\Controllers;

use Keygen;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Config;
use Illuminate\Encryption\Encrypter;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Model;
use Adldap\Laravel\Facades\Adldap;

use App\User;
use App\Account;
use App\Key;
use App\Password;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function login(Request $request)
    {
        $user = $request->get('user');
        $userinfo = User::where('remote_user', $user)->get();
        return response()->json(['data'=>$userinfo]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function getpassword($accountid)
    {
        $password = Account::where('id',$accountid)->value('password');
        return response()->json(['data'=>$password]);
    }
    public function store(Request $request)
    {
        $data = $request->only('remote_user','public_key','enc_private_key');

        $user = User::create($data);
        if($user){
            return response()->json(['data'=>$user]);
        }
    }

    public function sharepass(Request $request)
    {
        $data = $request->only('userid','accountid','enc_password','is_read_write','can_reshare');

        $password = new Password;

        $password->account_id = $request->get('accountid');
        $password->user_id = $request->get('userid');
        $password->password = $request->get('enc_password');
        $password->isreadwrite = $request->get('is_read_write');
        $password->can_reshare = $request->get('can_reshare');

        $sharedpass = Password::create($password);
        if($sharedpass)
            return response()->json(['status'=>1]);
        else
            return response()->json(['status'=>0]);
    }

    public function getusers()
    {
        $users = User::all();
        return response()->json(['users'=>$users]);
    }

    public function getinfo($id)
    {
        $userinfo = User::where('id', $id)->get();
        return response()->json(['user'=>$userinfo]);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($userid)
    {
        $user = User::where('id',$userid)->get();

        if($user){
            return response()->json(['data'=>$user]);
        }else{
            return response()->json(['data'=>0]);
        }

    }

    public function getaccounts($userid)
    {
        $user_id = User::where('id', $userid)->value('id');
        /*$accounts = DB::table('accounts') ->join('passwords','accounts.id','=','passwords.account_id') ->where('accounts.user_id',$user_id) ->get();*/
        $accounts = Account::where('user_id', $userid)->get();
        return Response()->json(['accounts'=>$accounts]);
    }

    public function sharedpasswords($userid)
    {
        //$passwords = Password::where('user_id',$userid)->get();
        $passwords = DB::table('passwords') ->join('accounts','accounts.id','=','passwords.account_id') ->where('passwords.user_id',$user_id) ->get();
        return Response()->json(['passwords'=>$passwords]);
    }

    public function updatepass(Request $request){
        $data=$request->only('accountid','password');
        $account = Account::find($request->get('accountid'));
        $account->password = $data["password"];
        if($account->save())
            return Response()->json(['data'=>1]);
        else
            return Response()->json(['data'=>0]);
    }

    public function account(Request $request)
    {
        $data = $request->only('userid','username','email','enc_password','website_url');

        $account['user_id'] = $request->get('userid');
        $account['uid'] = $request->get('username');
        $account['mail'] = $request->get('email');
        $account['site_url'] = $request->get('website_url');
        $account['password'] = $request->get('enc_password');

        if($newaccount=Account::create($account)){
            return Response()->json(['accountdata'=>$newaccount]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
