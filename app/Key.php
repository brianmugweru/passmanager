<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Key extends Model
{
    //
    protected $fillable = [
        'account_id','user_id','keys'
    ];
}
