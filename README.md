##PASSWORD MANAGER 

An application to do password management to be used by small enterprises where their passwords to various files will be encrypted and will be shared amongs't them with encryptions keys for encryption of various files.

### PREREQUISITES
Laravel
Angular2js
MYSQL

##INSTALLATION

A step by step procedure on how to install the application for development

get your local copy git clone \https://gitlab.com/brianmugweru/passmanager.git'

Make sure your have php 7.0 and all of its extensions installed, mysql installed, composer for php dependencies and apache as well.

cd into the project and run the following commands

    composer install 
    
    php artisan migrate
    
    php artisan serve
    

Open your browser and run localhost:8000


