const elixir = require('laravel-elixir');
 
require('laravel-elixir-browserify-official');
 
var bowerDir = './resources/assets/bower/';


elixir(mix => {
    mix.scripts([
        'jquery/dist/jquery.min.js',
        'bootstrap/dist/js/bootstrap.min.js'
        ], 'public/js/all.js', bowerDir)
        .copy(bowerDir+'angular/angular.min.js','public/app/lib/angular/angular.min.js')
        .copy(bowerDir+'lodash/dist/lodash.min.js', 'public/app/lib/angular/lodash.min.js')
        .copy(bowerDir+'angular-local-storage/dist/angular-local-storage.min.js', 'public/app/lib/angular/angular-local-storage.min.js')
        .copy(bowerDir+'angular-route/angular-route.min.js', 'public/app/lib/angular/angular-route.min.js')
        .copy(bowerDir+'restangular/dist/restangular.min.js', 'public/app/lib/angular/restangular.min.js')
        .copy(bowerDir+'angular-web-crypto/dist/ng-web-crypto.es5.min.js', 'public/app/lib/angular/ng-web-crypto.es5.min.js')
        .copy(bowerDir+'angularjs-crypto/public/js/lib/angularjs-crypto.js', 'public/app/lib/angular/ng-angularjs-crypto.js')
        .copy(bowerDir+'angularjs-crypto/public/js/lib/plugins/CryptoJSCipher.js', 'public/app/lib/angular/cryptojscipher.js')
});

