<?php

/* |--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::post('/api/auth/signup', 'UserController@store');
Route::get('/api/auth/check/{userid}', 'UserController@show');
Route::post('/api/account/create', 'UserController@account');
Route::post('/api/password/create', 'UserController@createpass');
Route::get('/api/accounts/get/{userid}', 'UserController@getaccounts');
Route::post('/api/getkey/', 'UserController@getkey');
Route::post('/api/updatepassword/', 'UserController@updatepass');
Route::get('/api/users', 'UserController@getusers');
Route::get('/api/user/get/{userid}', 'UserController@getinfo');
Route::post('/api/auth/login', 'UserController@login');
Route::post('/api/account/sharepassword', 'UserController@sharepass');
Route::get('/api/account/getpassword/{accountid}', 'UserController@getpassword');


