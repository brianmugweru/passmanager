var passwordManagerServices = angular.module('passwordManagerServices', [ 'LocalStorageModule','ngWebCrypto']);

passwordManagerServices.factory('userService', ['$http','localStorageService',function($http,localStorageService){
    function checkstoragesupport(){
        if(localStorageService.isSupported)
            return true;
        else
            return false;
    }

    function checkUserSession(key){
        var userid = localStorageService.get(key);
        if(typeof userid===undefined || userid===null)
            return false;
        else
            return true;
    }
    
    function getinfo(id,onSuccess,onError){
        $http.get('/api/user/get/'+id).
            then(function(response){
                onSuccess(response);
            },function(response){
                onError(response);
            });
    }

    function getpassword(accountid, onSuccess, onError){
        $http.get('/api/account/password'+accountid).
            then(function(response){
                onSuccess(response);
            },function(response){
                onError(response);
            });
    }

    function processaccount(userid, username, email, enc_password, website_url, onSuccess, onError){
        $http.post('/api/account/create',{
             userid:userid,
             username:username,
             enc_password:enc_password,
             email:email,
             website_url:website_url,
        }).then(function(response){
            onSuccess(response);
        },function(response){
            onError(response);
        });
    }

    function getusers(onSuccess, onError){
        $http.get('/api/users', {
        }).then(function(response){
            onSuccess(response);
        },function(response){
            onError(response);
        });
    }

    function updatepassword(accountid, password, onSuccess, onError){
        $http.post('/api/updatepassword', {
            accountid:accountid,
            password:password
        }).then(function(response){
            onSuccess(response);
        },function(response){
            onError(response);
        });
    }

    function getkey(userid, accountid, onSuccess, onError){
        $http.post('/api/getkey',{
            userid:userid,
            accountid:accountid
        }).then(function(response){
            onSuccess(response);
        },function(response){
            onError(response);
        });
    }

    function sharepassword(userid, accountid, enc_password,is_read_write,can_reshare,onSuccess,onError){
        $http.post('/api/account/sharepassword',{
            userid:userid,
            accountid:accountid,
            enc_password:enc_password,
            is_read_write:is_read_write,
            can_reshare:can_reshare,
        }).then(function(response){
            onSuccess(response);
        },function(response){
            onError(response);
        });
    }

    function check(userid, onSuccess, onError){
        $http.get('/api/auth/check/'+userid)
            .then(function(response){
                onSuccess(response);
            },function(response){
                onError(response);
        });
    }

    function getaccounts(userid, onSuccess, onError){
        $http.get('/api/accounts/get/'+userid)
            .then(function(response){
                onSuccess(response);
            },function(response){
                onError(response);
        });
    }

    function signup(remoteuser, encryptedkey, publicKey, onSuccess, onError){
        $http.post('/api/auth/signup', { 
            remote_user:remoteuser,
            public_key:publicKey,
            enc_private_key:encryptedkey,
        }).then(function(response){
            onSuccess(response);
        },function(response){
            onError(response);
        });
    }

    function login(remoteuser, onSuccess, onError){
        $http.post('/api/auth/login', { 
            user:remoteuser 
        }).then(function(response){
            onSuccess(response);
        },function(response){
            onError(response);
        });
    }

    function logout(){
        localStorageService.clearAll();
    }

    function getCurrentToken(){
        return localStorageService.get('token');
    }

    return {
        getaccounts:getaccounts,
        processaccount:processaccount,
        checkUserSession:checkUserSession,
        check:check,
        checkstoragesupport:checkstoragesupport,
        signup: signup,
        login: login,
        logout: logout,
        getkey: getkey,
        updatepassword:updatepassword,
        getCurrentToken: getCurrentToken,
        getusers:getusers,
        getinfo:getinfo,
        sharepassword:sharepassword,
        getpassword:getpassword
    }

}]);


