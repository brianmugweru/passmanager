'use strict';

var NgWebPBKDF2 = function(){
    function NgWebPBKDF2(){
        this.stringToArrayBuffer = function(string) {
            var encoder = new TextEncoder("utf-8");
            return encoder.encode(string);
        };
        this.arrayBufferToHexString = function(arrayBuffer) {
            var byteArray = new Uint8Array(arrayBuffer);
            var hexString = "";
            var nextHexByte;

            for (var i=0; i<byteArray.byteLength; i++) {
                nextHexByte = byteArray[i].toString(16);  // Integer to base 16
                if (nextHexByte.length < 2) {
                    nextHexByte = "0" + nextHexByte;     // Otherwise 10 becomes just a instead of 0a
                }
                hexString += nextHexByte;
            }
            return hexString;
        }
        this.isDefined = function (variable) {
            if (typeof variable === 'undefined' || variable === null) {
                return false;
            }
            return true;
        };
    }
    return NgWebPBKDF2;
}();

var ngWebConstants = {
    class: {
        PBKDF2 : 'PBKDF2'
    },
    format: {
        RAW: 'raw'
    }
};
angular.module('ngWebPBKDF2', []);
angular.module('ngWebPBKDF2').provider('$pbkdf2', function($injector) {
    var _this = this;

    var crypto = window.crypto;
    if(!crypto.subtle){
        throw 'ng-web-crypto: not supported by browser';
    }
    if(!window.TextEncoder || !window.TextDecoder) {
        alert("Your browser does not support the Encoding API! This page will not work.");
        return;
    }

        var tools = $injector.instantiate(NgWebPBKDF2);

    var keys = [];
    var cryptoKeys = [];

    var getKey = function getKey(kName){
        for(var c = 0; c > keys.length; c++){
            if(keys[c].name == kName){
                return keys[c];
            }
        }
        return -1;
    };

    this.checkKey= function(kName){
        return getKey(kName) != -1;
    };

    var getCryptoKey = function getCryptoKey(kname){
        for (var c = 0; c < cryptoKeys.length; c++) {
            if (cryptoKeys[c].name == kName) {
                return cryptoKeys[c];
            }
        }
        return -1;
    };


    var defaultKey = null,
        defaultCryptoKey= null;

    this.generateKey = function(options){
        var promise = new Promise(function(resolve, reject){
            if(!tools.isDefined(options.name)){
                reject('key Name is required for generating.');
            }
            if(getKey(options.name) != -1){
                reject("key name " + options.name + " already in use.");
            }
            if(!tools.isDefined(options.iterations)){
                options.iterations = 2000;
            }
            crypto.subtle.importKey(
                ngWebConstants.format.RAW, 
                tools.stringToArrayBuffer(options.name),
                {
                    name:ngWebConstants.class.PBKDF2
                },
                false,
                ['deriveKey', 'deriveBits'])
                .then(function(key){
                    crypto.subtle.deriveKey(
                        {
                            "name":ngWebConstants.class.PBKDF2,
                            "salt":tools.stringToArrayBuffer(options.eKey),
                            "iterations":2000,
                            "hash":{name:"SHA-512"},
                        },
                        key,
                        {"name":"AES-CBC","length":256},
                        true,
                        ["encrypt","decrypt"],
                    ).then(function(aesKey){
                        return crypto.subtle.exportKey(ngWebConstants.format.RAW, aesKey);
                    }).then(function(keyBytes) {
                        var hexBytes = tools.arrayBufferToHexString(keyBytes);
                        keys.push({
                            class:ngWebConstants.class.PBKDF2,
                            name:options.name,
                            key:hexBytes
                        });
                        resolve(hexBytes);
                    })
                }).catch(function(err){
                    reject(err);
                });
        });
        promise.success = function(fn){
            promise.then(function(name){
                fn(name);
            });
            return promise;
        };
        promise.error = function(fn){
            promise.then(null, function(name){
                fn(name);
            });
            return promise;
        };
        return promise;
    };
    this.getDefaultKeys = function () {
        return {
            ecdh: defaultKey,
            crypto: defaultCryptoKey
        };
    };
    this.$get = function(){
        return{
            tools: {
                stringToArrayBuffer: function stringToArrayBuffer(string){
                    return tools.stringToArrayBuffer(string);
                },
                arrayBufferToHexString: function arrayBufferToHexString(arrayBuffer){
                    return tools.arrayBufferToHexString(arrayBuffer);
                }
            },
            generate: function generate(options){
                return _this.generateKey(options);
            }
        }
    };
});

