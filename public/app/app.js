var passwordManagerApp = angular.module('passwordManagerApp', [
        'ngRoute',
        'ngWebCrypto',
        'ngWebPBKDF2',
        'passwordManagerAppControllers',
        'passwordManagerServices',
        'LocalStorageModule',
        'angularjs-crypto'
]);

passwordManagerApp.config(['$routeProvider','$webCryptoProvider', function($routeProvider,$webCryptoProvider){
    $routeProvider.
        when('/masterkey', {
            templateUrl:'partials/enterkey.html',
            controller:'EnterKeyController'
        }).
        when('/newkey', {
            templateUrl:'partials/newkey.html',
            controller:'NewKeyController'
        }).
        when('/auth',{
            templateUrl:'partials/home.html',
            controller:'HomeController'
        }).
        when('/accounts', {
            templateUrl:'partials/account.html',
            controller:'AccountController'
        }).
        when('/',{
            templateUrl:'partials/index.html',
            controller:'MainController'
        }).
        when('/logout',{
            templateUrl:'partials/index.html',
            controller:'LogoutController'
        }).
        otherwise({
            redirectTo:'/'
        });
}]);


