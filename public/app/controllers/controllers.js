var passwordManagerAppControllers = angular.module('passwordManagerAppControllers', []);

passwordManagerAppControllers.controller('NewKeyController',['$scope','$pbkdf2','$webCrypto','localStorageService','$location','userService',function($scope,$pbkdf2,$webCrypto, localStorageService, $location,userService){
    $scope.tryalice = function(){
        $webCrypto.generate({name:'alice'})
            .success(function(aliceKeyName){
                console.log($webCrypto.export(aliceKeyName));
            });
    };
    $scope.signup=function(){
        window.crypto.subtle.generateKey(
        {
            name:"RSA-OAEP",
            modulusLength:2048,
            publicExponent:new Uint8Array([0x01,0x00,0x01]),
            hash:{name:"SHA-256"}
        },
        true,
        ['encrypt','decrypt'])
        .then(function(key){
            window.crypto.subtle.exportKey(
                "spki",
                key.publicKey
            ).then(function(extpublickey){
                var pbckey = $webCrypto.tools.ABToHS(new Uint8Array(extpublickey));
                window.crypto.subtle.exportKey(
                    "pkcs8",
                    key.privateKey
                ).then(function(extprivatekey){
                    var prtkey = $webCrypto.tools.ABToHS(new Uint8Array(extprivatekey));
                    console.log("exported public key in hexadecimal",pbckey);
                    console.log("exported private key in hexadecimal",prtkey);
                    //Encrypt private key and hash with common crypto method with masterkey then send in encrypted form to db
                    //Try out encryption and decryption to confirm same key configuration
                    $pbkdf2.generate(
                        {
                            name:$scope.masterkey
                        }
                    ).success(function(pbkdfkey){
                        console.log("generate pbkdf2 key",pbkdfkey);
                        var base64key = CryptoJS.enc.Hex.parse(pbkdfkey);
                        var iv = CryptoJS.enc.Hex.parse(pbkdfkey);

                        var encrypted = CryptoJS.AES.encrypt(
                            prtkey,
                            base64key,
                            { iv: iv }
                        );

                        var encryptedkey = encrypted.ciphertext.toString(CryptoJS.enc.Base64);
                        console.log("primary key has been encrypted with users master key and can be posted to database");
                        console.log("encrpted primary key:",encryptedkey);
                        console.log("users public key:",pbckey);
                        console.log("users private key:",prtkey);

                        /**  
                         *   This section is to be commented out, it should test whether encrypted key when decrypted matches
                         *   the public key exported before encryption
                         **/
                         var cipherParams = CryptoJS.lib.CipherParams.create({
                             ciphertext: CryptoJS.enc.Base64.parse(encryptedkey)
                         }); 

                         var decrypted = CryptoJS.AES.decrypt(
                             cipherParams,
                             $rootScope.base64Key,
                             { iv: $rootScope.iv }
                         );
                         var decryptedkey = decrypted.toString(CryptoJS.enc.Utf8);

                         if(decryptedkey === prtkey)
                             console.log("key confirmation after decryption confirms a match of the encrypted private key");
                         /**
                          *  End of section that should be cleared after testing
                          *
                          **/

                        localStorageService.set('prtkey',prtkey);
                        localStorageService.set('pbckey',pbckey);
                        userService.signup(remoteuser, encryptedkey, pbckey,
                            function(response){
                                console.log(response);
                                localStorageService.set('id',response.data.data.id);
                                $location.path('/auth');
                            },function(response){
                                localStorageService.clearAll();
                                alert("sorry, something wen't wrong with the signup, check console");
                                console.log(response);
                            }
                        );
                    }).error(function(err){
                        console.log(err);
                    });
                }).catch(function(err){
                    console.log(err);
                });
            }).catch(function(err){
                console.log(err);
            });
        }).catch(function(err){
            console.log(err);
        });
    }
}]);

passwordManagerAppControllers.controller('EnterKeyController',['$pbkdf2','$webCrypto','userService','$scope', 'localStorageService','$location', function($pbkdf2,$webCrypto,userService, $scope, localStorageService, $location){
    $scope.login=function(){
        $pbkdf2.generate(
            {
                name:$scope.masterkey
            }
        ).success(function(pbkdfkey){
            console.log("generate pbkdf2 key",pbkdfkey);
            var base64key = CryptoJS.enc.Hex.parse(pbkdfkey);
            var iv = CryptoJS.enc.Hex.parse(pbkdfkey);

            userService.login(remoteuser,
                function(response){
                    console.log(response);
                    var encryptedkey = response.data.data.enc_privatekey;
                    var publickey = response.data.data.public_key;

                    var cipherParams = CryptoJS.lib.CipherParams.create({
                         ciphertext: CryptoJS.enc.Base64.parse(encryptedkey)
                     }); 

                     var decrypted = CryptoJS.AES.decrypt(
                         cipherParams,
                         $rootScope.base64Key,
                         { iv: $rootScope.iv }
                     );
                     var decryptedkey = decrypted.toString(CryptoJS.enc.Utf8);
                     console.log('decrypted private key', decryptedkey);

                     localStorageService.set('prtkey',decryptedkey);
                     localStorageService.set('pbtkey',publickey);
                     localStorageService.set('id',response.data.data.id);
                     $location.path('/auth');
                },function(response){
                    alert("hello dude, check your logs, something is wrong");
                    console.log(response);
                    $location.path('/masterkey');
                }
            );
        }).error(function(error){
            console.log(error);
        });
    };
}]);

passwordManagerAppControllers.controller('AccountController', ['userService','$webCrypto','$rootScope','localStorageService','$scope','$location', function(userService, $webCrypto, $rootScope,localStorageService, $scope, $location){
    userService.getaccounts(localStorageService.get('id'),function(response){
        console.log(response.data.accounts);
        $scope.accounts = response.data.accounts;
        $location.path('/accounts');
    },function(response){
        console.log(response);
        alert('error in code, check in console');
    });

    $scope.modal=function(id){
        $scope.accountid = id;
    }

    $scope.popshare = function(id,spassword){
        var prtkey = localStorageService.get('prtkey');
        var cryptoprtkey = $webCrypto.tools.HSToAB(prtkey);
        window.crypto.subtle.importKey(
            "pkcs8",
            cryptoprtkey,
            {
                name:"RSA-OAEP",
                hash:{name:"SHA-256"}
            },
            false,
            ["decrypt"]
        ).then(function(decryptionkey){
            window.crypto.subtle.decrypt(
                {
                    name:"RSA-OAEP",
                },
                decryptionkey,
                new Uint8Array(password)
            ).then(function(decryted){
                console.log($webCrypto.tools.ABtoString(new Uint8Array(decrypted)));
                $scope.spassword = descrString; 
                $scope.accountid = id;
            }).catch(function(err){
                console.log(err);
            });
        }).catch(function(err){
            console.log(err);
        })
        userService.getusers(
            function(response){
                $scope.users = response.data.users;
            },function(response){
                console.log(response);
            }
        );
    }

    $scope.tryalice = function(){
        $webCrypto.generate({name:'alice'})
            .success(function(aliceKeyName){
                console.log($webCrypto.export(aliceKeyName));
            });
    };

    $scope.share = function(){
        $scope.usersarray = [];
        var shpass = $scope.spassword;
        var account = $scope.accountid;
        console.log('sharepassword:'+shpass);
        console.log('account id:'+account);
        var count = true;
        angular.forEach($scope.users, function(user){
            if(!!user.selected && user.id!==""){
                $scope.usersarray.push(user.id);
            }else{
                count=false;
            }
        });

        if($scope.usersarray.length == 0)
            alert("sorry, no values chosen");
        else{
            angular.forEach($scope.usersarray, function(user,x){
                userService.getinfo(user,
                   function(response){
                       var public_key = response.data.user["0"].public_key;
                       var userid = response.data.user["0"].id;
                       var cryptopbckey = $webCrypto.tools.HSToAB(public_key);
                       window.crypto.subtle.importKey(
                           "spki",
                           cryptopbckey,
                           {
                               name:"RSA-OAEP",
                               hash:{name:"SHA-256"},
                           },
                           false,
                           ["encrypt"]
                       ).then(function(enckey){
                           console.log(enckey);
                           window.crypto.encrypt(
                               {
                                   name:"RSA-OAEP"
                               },
                               enckey,
                               tools.StringtoAB(shpass)
                           ).then(function(encrypted){
                               var encrypted_in_hs = $webCrypto.tools.ABToHS(new Uint8Array(encrypted));
                               console.log("encrypted(hexadecimal string):", encrypted_in_hs);
                               console.log(new Uint8Array(encrypted));
                               alert(new Uint8Array(encrypted));
                               userService.sharepassword(userid, account, encrypted_in_hs,$scope.readwrite,$scope.reshare,
                                   function(response){
                                       alert(response);//check out saving variable state with static keyword to save variables
                                       console.log(response);
                                   },function(response){
                                       alert(response);
                                   }
                               );
                           }).catch(function(err){
                               console.log(err);
                           });
                       }).catch(function(err){
                           console.log(err);
                       });
                   },function(response){
                       console.log(response);
                       alert("sorry, error in your code");
                   }
               );
            });
        };
    }

    $scope.changepassword=function(){
        if($scope.password!==$scope.confirm_password){
            alert("sorry, passwords do not match");
            $location.path('/accounts');
        }
        userService.getpassword(accountid,
            function(response){
                var prtkey_in_ab_form = $webCrypto.tools.HSToAB($scope.prtkey);
                var encryptedpassword = response.data.data.password;
                window.crypto.subtle.imporKey(
                    "pkcs8",
                    prtkey_in_ab_form,
                    {
                        name:"RSA-OAEP",
                        hash:{name:"SHA-256"},
                    },
                    false,
                    ["decrypt"]
                ).then(function(deckey){
                    console.log(deckey);
                    window.subtle.crypto.decrypt(
                        {
                            name:"RSA-OAEP"
                        },
                        deckey,
                        new Uint8Array(response.data.data.password)
                    ).then(function(decrypted){
                        var decryptedpass = $webCrypto.tools.ABtoString(new Uint8Array(decrypted));
                        console.log(decryptedpass);
                        if($scope.p_password!==decryptedpass){
                            console.log("password do not match");
                            alert("Wrong password in previous password field");
                            $location.path("/accounts");
                        }else{
                            var pbckey_in_ab_format = $webCrypto.tools.HSToAB($scope.pbckey);
                            window.crypto.subtle.importKey(
                                "spki",
                                pbckey_in_ab_format,
                                {
                                    name:"RSA-OAEP",
                                    hash:{name:"SHA-256"},
                                },
                                false,
                                ["encrypt"]
                            ).then(function(enckey){
                                console.log(enckey);
                                window.crypto.subtle.encrypt(
                                    {
                                        name:"RSA-OAEP"
                                    },
                                    enckey,
                                    $webCrypto.StringtoAB($scope.password)
                                ).then(function(encrypted){
                                    console.log(new Uint8Array(encrypted));
                                    var enc_in_hs = $webCrypto.tools.ABToHS(new Uint8Array(encypted));
                                    userService.updatePassword(accountid,
                                        function(response){
                                            alert("successfully updated password");
                                            console.log(response);
                                            $location.path('/accounts');
                                        },function(response){
                                            console.log(response);
                                        }
                                    );
                                }).catch(function(err){
                                    console.log(err);
                                });
                            }).catch(function(err){
                                    console.log(err);
                            });
                        }
                    }).catch(function(err){
                        console.log(err);
                    });
                }).catch(function(err){
                    console.log(err);
                });
            },function(response){
                console.log(response);
            }
        );
    }

    $scope.decrypt=function(password){
        var prtkey = localStorageService.get('prtkey');
        var cryptoprtkey = $webCrypto.tools.HSToAB(prtkey);
        window.crypto.subtle.importKey(
            "pkcs8",
            cryptoprtkey,
            {
                name:"RSA-OAEP",
                hash:{name:"SHA-256"}
            },
            false,
            ["decrypt"]
        ).then(function(decryptionkey){
            window.crypto.subtle.decrypt(
                {
                    name:"RSA-OAEP",
                },
                decryptionkey,
                new Uint8Array(password)
            ).then(function(decryted){
                console.log($webCrypto.tools.ABtoString(new Uint8Array(decrypted)));
                alert($webCyrpto.tools.ABtoString(new Uint8Array(decrypted)));
            }).catch(function(err){
                console.log(err);
            });
        }).catch(function(err){
            console.log(err);
        })
    }
}]);

passwordManagerAppControllers.controller('LogoutController', ['$scope','localStorageService','$location', 'userService',function($scope,localStorageService,$location, userService){
    if(userService.logout){
        localStorageService.clearAll();
        $location.path('/masterKey');
    }
    else
        alert("sorry bro");
}]);

passwordManagerAppControllers.controller('MainController',['$scope','$location','localStorageService', 'userService',function($scope, $location, localStorageService, userService){
    if(userService.checkstoragesupport){
        userService.check(localStorageService.userid,//check for remote user key pair from db and decide the next page
            function(response){
                if(response.data.data == 0)
                    $location.path('/newkey');
                else
                    $location.path('/masterkey');
            },function(response){
                alert("Something is seriously wrong with your backend, consult your system admin");
                $location.path('/masterkey');
            }
        )
    }else{
        $scope.sup="Local storage not supported by your browser";
    }
}]);

passwordManagerAppControllers.controller('HomeController',['cfCryptoHttpInterceptor','$location','$rootScope','$scope','$rootScope','$webCrypto','localStorageService','userService',function(cfCryptoHttpInterceptor,$location,$rootScope,$scope,$rootScope,$webCrypto, localStorageService, userService){
    var prtkey = localStorageService.get('prtkey');
    var pbckey = localStorageService.get('pbckey');
    var pbckey_in_ab_format = $webCrypto.tools.HSToAB(pbckey);
    $scope.process = function(){
        window.crypto.importKey(
            "spki",
            pbckey_in_ab_format,
            {
                name:"RSA-OAEP",
                hash:{name:"SHA-256"},
            },
            false,
            ["encrypt"]
        ).then(function(enckey){
            console.log(enckey);
            window.crypto.subtle.encrypt(
                {
                    name:"RSA-OAEP"
                },
                enckey,
                $webCrypto.tools.StringtoAB($scope.password)
            ).then(function(encrypted){
                console.log(new Uint8Array(encrypted));
                console.log("encrypted",tools.ABToHS(new Uint8Array(encrypted)));
                var enchs = $webCrypto.tools.ABToHS(new Uint8Array(encrypted));
                userService.processaccount(userid, $scope.username, $scope.email, enchs, $scope.website_url, 
                    function(response){
                        console.log(response);
                    },function(response){
                        console.log(response);
                    }
                );
            }).catch(function(err){
                console.log(err);
            });
        }).catch(function(err){
            console.log(err);
        });
    }
}]);


